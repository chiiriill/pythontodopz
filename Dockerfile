FROM python:3.9-slim

WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt --no-cache-dir
RUN apt-get update && apt-get install -y vim nano

#COPY . /app
#ADD . /app

ENTRYPOINT ["python", "-m", "cli"]