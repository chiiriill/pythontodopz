"""
    Module to get config from .env
"""

import os
from dataclasses import dataclass
from dotenv import load_dotenv


@dataclass
class Config:
    """
        Class for config
    """
    path_todo: list
    editor: str


def get_config() -> Config:
    """
        Get config from .env file and return Config
    """
    load_dotenv()

    path_todo = os.getenv('PATH_TODO')
    editor = os.getenv('EDITOR')
    if path_todo is None and editor is None:
        with open(".env", "w", encoding="utf-8") as file:
            file.write('PATH_TODO="data.json"\nEDITOR=nano')
        path_todo = "data.json"
        editor = "nano"
    elif path_todo is None:
        with open(".env", "w", encoding="utf-8") as file:
            file.write(f'PATH_TODO="data.json"\nEDITOR={editor}')
        path_todo = "data.json"
    elif editor is None:
        with open(".env", "w", encoding="utf-8") as file:
            file.write(f'PATH_TODO="{path_todo}"\nEDITOR=nano')
        editor = "nano"
    return Config(path_todo=path_todo.split(), editor=editor)


def add_new_todo(todos: list, new_todo: str) -> str:
    """Add new todo in config file"""
    todos.append(new_todo)
    new_path_todo = ' '.join(todos)
    with open(".env", "w", encoding="utf-8") as file:
        file.write(f'PATH_TODO="{new_path_todo}"')
    return new_todo


if __name__ == '__main__':
    con = get_config()
    print(con.path_todo)
    print(con.editor)
