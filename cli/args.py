"""
    Module for function for flag in CLI TodoJournal
"""


def delete_entries(todo):
    """
        Interactive remove entries
    """
    delete_entries_index = []
    for k, entry in enumerate(todo.entries):
        answer = input(f"Вы хотите удалить запись: {entry} [Y/n]: ").lower()
        if answer == "y":
            delete_entries_index.append(k)
    for index in delete_entries_index[::-1]:
        todo.remove_entry(index)
    print(f"Записи закончились, всего удалено {len(delete_entries_index)} записей!")


def choice_todo(todos: list) -> str:
    """Choice todos user"""
    if len(todos) == 1:
        return todos[0]
    for k, v in enumerate(todos, 1):
        print(f"{k}: {v}")
    while True:
        choice = input("Выберите номер todo list для которого произвести изменения: ")
        try:
            num = int(choice) - 1
            if num == -1:
                raise IndexError
            todo_choice = todos[num]
        except IndexError:
            print("Такого номера не существует!")
            continue
        except ValueError:
            print("Вы написали некорректный вариант, напишите числовой номер todo list(Пример 1)")
            continue
        while True:
            choice_answer = input(f"Вы выбрали {todo_choice} [Y/n]: ").lower()

            if choice_answer == "y":
                return todo_choice
            if choice_answer == "n":
                print("Повторите выбор todo")
                break
            print("Некорректный вариант выбора. Повторите выбор [Y/n]")
