"""
    CLI TodoJournal
"""
import os
import logging
from logging.handlers import TimedRotatingFileHandler
import click
from datetime import datetime

from src.TodoJournal import TodoJournal
from src.exceptions import ExtensionFileTodoError
from src.exceptions import TodoNotValidJsonSchemeError
from cli.args import delete_entries
from cli.args import choice_todo
from cli.config import get_config
from cli.config import add_new_todo

config = get_config()

DATE_FORMAT = "%Y-%m-%d %H:%M"

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

log_handler = TimedRotatingFileHandler(filename='logs.log', when='D', interval=1, backupCount=7)
log_handler.setLevel(logging.DEBUG)

file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s')
log_handler.setFormatter(file_formatter)

logger.addHandler(log_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.ERROR)
console_formatter = logging.Formatter('%(message)s')
console_handler.setFormatter(console_formatter)
logger.addHandler(console_handler)


@click.command()
@click.argument("arg", required=False)
@click.option("--delete", "-d", is_flag=True, help="Interactive delete entries from todos")
@click.option("--output", is_flag=True, help="Print all entries from todos")
@click.option("--todo", "-t", is_flag=False, default=None, help="Choice todo")
@click.option("--edit", is_flag=True, help="Editor todos list")
def main(delete=False, output=False, todo=None, edit=False, arg=None):
    """Main function"""
    if not delete and not output and todo is None and not edit and arg is None:
        return

    todos = config.path_todo
    if todo is None:
        path_todo = choice_todo(todos)
    else:
        if todo not in todos:
            if not todo.endswith(".json"):
                logger.error(f"Invalid todo format: {todo}")
                raise ExtensionFileTodoError("Todo name must be ended to .json")
            path_todo = add_new_todo(todos, todo)
        else:
            path_todo = todo
    todo = TodoJournal(path_todo)

    if arg is not None:
        while True:
            try:
                date = input("Введите дату в формате 2023-02-13 18:00 : ")
                datetime.strptime(date, DATE_FORMAT)
                break
            except ValueError:
                logger.warning(f"Invalid date set in todo")
                print("Дата не удовлетворяет формату %Y-%m-%d %H:%M")
        logger.info(f"Create new todo: {arg}")
        todo.add_entry(arg, date)
        print(f"Add new entry to {arg}")

    if edit:
        while True:
            os.system(f'{config.editor} {path_todo}')
            try:
                todo.validate_json()
            except TodoNotValidJsonSchemeError:
                logger.warning("Todo after change have invalid format!")
                print("Не правильный формат json повторите редактирование!")
                input("Нажмите [ENTER], чтобы вернуться к редактированию")
                continue
            logger.info(f"User update todo")
            print("Данные успешно изменены!")
            todo.update_data()
            break
    if output:
        logger.info(f"Print all entries from todo")
        print(todo)
    if delete:
        logger.info(f"User interactive delete todo")
        delete_entries(todo)


if __name__ == "__main__":
    main()
