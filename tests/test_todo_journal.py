"""
    Tests TodoJournal class
"""
import pytest

from src.TodoJournal import TodoJournal
from src.exceptions import TodoJournalError
from src.exceptions import ExtensionFileTodoError
from src.exceptions import TodoIsNotExistError
from tests.config import PATH_TO_TEST
from tests.config import NAME_TODO


def test_init(tests_env):
    """Test init class TodoJournal"""
    _pytest_fixtures = [tests_env]
    expected_entries = []

    todo = TodoJournal(PATH_TO_TEST, NAME_TODO)
    entries = todo.entries
    name = todo.name

    assert entries == expected_entries
    assert name == NAME_TODO


def test_length(todo_journal_with_2_entries):
    """Test __len__ class TodoJournal"""
    _pytest_fixtures = [todo_journal_with_2_entries]
    todo = TodoJournal(PATH_TO_TEST, NAME_TODO)
    assert len(todo) == 2


def test_getitem_success(todo_journal_with_2_entries):
    """Test __getitem__ class TodoJournal"""
    _pytest_fixtures = [todo_journal_with_2_entries]
    todo = TodoJournal(PATH_TO_TEST, NAME_TODO)
    assert todo[0] == {
        "date": "2023-02-3 19:00",
        "label": "foo"
    }
    assert todo.first == {
        "date": "2023-02-3 19:00",
        "label": "foo"
    }
    assert todo.last == {
        "date": None,
        "label": "foo"
    }


def test_getitem_failed_index(todo_journal_with_2_entries):
    """Test __getitem__ with failed index class TodoJournal"""
    _pytest_fixtures = [todo_journal_with_2_entries]
    todo = TodoJournal(PATH_TO_TEST, NAME_TODO)
    with pytest.raises(TodoJournalError):
        assert todo[5]


def test_add_entry_with_date(tests_env):
    """Test add entry class TodoJournal"""
    _pytest_fixtures = [tests_env]
    todo = TodoJournal(PATH_TO_TEST, NAME_TODO)
    todo.add_entry("Go", "2023-02-3 19:00")
    assert todo[0]["date"] == "2023-02-3 19:00"
    assert todo[0]["label"] == "Go"


def test_add_entry_without_date(tests_env):
    """Test add entry without date class TodoJournal"""
    _pytest_fixtures = [tests_env]
    todo = TodoJournal(PATH_TO_TEST, NAME_TODO)
    todo.add_entry("Go")
    assert todo[0]["date"] is None
    assert todo[0]["label"] == "Go"


def test_failed_attr(tests_env):
    """Test failed attribute class TodoJournal"""
    _pytest_fixtures = [tests_env]
    todo = TodoJournal(PATH_TO_TEST, NAME_TODO)
    with pytest.raises(AttributeError):
        assert todo.gogd


def test_delete_value_error(tests_env):
    """Test delete entry with valueerror index class TodoJournal"""
    _pytest_fixtures = [tests_env]
    todo = TodoJournal(PATH_TO_TEST)
    with pytest.raises(ValueError):
        todo.remove_entry("dsf")


def test_delete_success(tests_env):
    """Test delete success class TodoJournal"""
    _pytest_fixtures = [tests_env]
    todo = TodoJournal(PATH_TO_TEST)
    todo.add_entry("Go", "2023-02-3")
    todo.remove_entry(0)
    assert len(todo) == 0


def test_delete_index_not_exist(tests_env):
    """Test delete with index not exist class TodoJournal"""
    _pytest_fixtures = [tests_env]
    todo = TodoJournal(PATH_TO_TEST)
    with pytest.raises(TodoIsNotExistError):
        todo.remove_entry(1)


def test_extension():
    """Test name file must be .json class TodoJournal"""
    with pytest.raises(ExtensionFileTodoError):
        TodoJournal("jdoasjsa", NAME_TODO)


def add(a, b):
    """
    Add two numbersFunction for parametrize test
    """
    return a + b


@pytest.mark.parametrize("a, b, expected", [(1, 2, 3), (0, 0, 0), (-1, 1, 0)])
def test_add(a, b, expected):
    """Example parametrize test"""
    assert add(a, b) == expected
