"""
    Pytest's fixtures
"""
import os
import json
import pytest

from tests.config import PATH_TO_TEST


@pytest.fixture()
def tests_env():
    """Создание файла перед использованием, удаление после."""
    if not os.path.exists("data"):
        os.mkdir("data")

    if os.path.exists(PATH_TO_TEST):
        os.remove(PATH_TO_TEST)

    yield

    os.remove(PATH_TO_TEST)

    os.rmdir("data")


@pytest.fixture()
def todo_journal_with_2_entries():
    """
        Fixtures created two entries
    """
    if not os.path.exists("data"):
        os.mkdir("data")

    if os.path.exists(PATH_TO_TEST):
        os.remove(PATH_TO_TEST)

    with open(PATH_TO_TEST, "w", encoding="utf-8") as f:
        json.dump(
            {
                "name": "test",
                "todos": [{
                    "date": "2023-02-3 19:00",
                    "label": "foo"
                },
                    {
                        "date": None,
                        "label": "foo"
                    }]
            },
            f,
            indent=4,
            ensure_ascii=False)

    yield

    os.remove(PATH_TO_TEST)

    os.rmdir("data")
