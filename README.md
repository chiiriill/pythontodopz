# TodoJournal

---

Модуль предназначен для введения todo записей с использованием json-файла. А также CLI приложение.

## Install

---

```bash
git clone git@gitlab.com:chiiriill/python-todo.git
cd python-todo
pip install -r requirements.txt
```

Load docker images by archive

```shell
docker load -i <имя_файла.tar>
```

## Quickstart

---

Using local

```shell
python3 -m cli --help
```

Using docker

```shell
docker run -it --rm -v $(pwd):/app test_env --help
```

## Run

---

```
python3 -m cli OPTIONS SLUSH_FILE

Options:
  -d, --delete     Interactive delete entries from todos
  --output         Print all entries from todos
  -t, --todo TEXT  Choice todo
  --edit           Editor todos list
  --help           Show this message and exit.

```

OPTIONS:

-  -d, --delete     Interactive delete entries from todos
-  --output         Print all entries from todos
-  -t, --todo TEXT  Choice todo
-  --edit           Editor todos list
-  --help           Show this message and exit.

## Tests

---

```bash
cd tests
pytest
pytest --cov=. --cov-report html
```

## Relocate docker images

Save archive

```shell
docker save <имя_образа> -o <имя_файла.tar>
```

