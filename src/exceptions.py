"""Module with exceptions used in project"""


class TodoJournalError(Exception):
    """The file extension is not json"""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class ExtensionFileTodoError(Exception):
    """The file extension is not json"""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class PathTodoFileError(Exception):
    """Error in path todos"""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class TodoIsNotExistError(Exception):
    """Error in path todos"""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class TodoNotValidJsonSchemeError(Exception):
    """Error in todos scheme format"""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)
