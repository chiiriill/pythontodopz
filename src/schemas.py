"""
    Module containing schemas used in todos application
"""
# pylint: disable=no-name-in-module
# pylint: disable=no-self-argument
from typing import List
from typing import Optional
from pydantic import BaseModel, validator, ValidationError
from datetime import datetime

DATE_FORMAT = "%Y-%m-%d %H:%M"


class Todo(BaseModel):
    """Schema for one todo user"""
    date: Optional[str]
    label: str

    @validator('date')
    def validate_date(cls, v: str) -> str:
        try:
            datetime.strptime(v, DATE_FORMAT)
            return v
        except ValueError:
            raise ValueError()


class TodoList(BaseModel):
    """Schema for TodosJournal object in json file"""
    name: str
    todos: List[Todo]
