# Ignored what name module in camel case font
# pylint: disable=invalid-name
"""
Todos Journal CLI
"""
import json
import os
from typing import Iterator

import pydantic

from src.schemas import TodoList
from src.exceptions import ExtensionFileTodoError
from src.exceptions import PathTodoFileError
from src.exceptions import TodoIsNotExistError
from src.exceptions import TodoNotValidJsonSchemeError
from src.exceptions import TodoJournalError
from src.schemas import TodoList


class TodoJournal:
    """A class to represent a todos list

    Attributes:
        name: The name of the todos list
        path_todo: The path to the todos file
    """

    def __init__(self, path_todo: str, name: str = "Undefined"):
        """Constructor for TodoJournal

        Exceptions:
            ExtensionFileTodoError: Extension file is not json
        """
        self.path_todo = path_todo
        self.name = name

        if not path_todo.endswith(".json"):
            raise ExtensionFileTodoError("File must have .json extension")

        if not os.path.exists(path_todo):
            self._create()

        self.entries = self._parse()["todos"]

    def __len__(self):
        return len(self.entries)

    def __str__(self):
        res = f"Todos {self.name}: {len(self.entries)} entries\n"
        for k, entry in enumerate(self.entries, 1):
            res += f"{k}: {entry['label']}\ntime: {entry['date']}\n\n"
        return res

    def __getitem__(self, item):
        try:
            entry = self.entries[item]
        except IndexError:
            raise TodoJournalError(f"Entries with index {item} not exist!") from None
        return entry

    shortcut_names = {"first": 0, "last": -1}

    def __getattr__(self, item):
        index = self.shortcut_names.get(item, None)
        if index is not None:
            return self.entries[index]

        cls = type(self)
        raise AttributeError(f"{cls.__name__} object has no attribute {item}")

    def __setattr__(self, name, value):
        error_msg = ''
        if name in self.shortcut_names:
            error_msg = f"readonly attribute {name}"
        if error_msg:
            raise AttributeError(error_msg)
        super().__setattr__(name, value)

    def __iter__(self) -> Iterator:
        return iter(self.entries)

    def validate_json(self):
        """Validate format json file"""
        now_scheme = str(self._parse()).replace("\'", "\"")
        try:
            TodoList.parse_raw(now_scheme)
        except pydantic.ValidationError:
            raise TodoNotValidJsonSchemeError("Error format todo") from None

    def add_entry(self, new_entry: str, date: str = "") -> None:
        """
        Add new todos

        Args:
            new_entry: new todos
            date: date end todos
        """

        new_todo = {"label": new_entry, "date": date if len(date.strip()) != 0 else None}

        self.entries.append(new_todo)

        new_data: TodoList = {
            "name": self.name,
            "todos": self.entries,
        }

        self._update(new_data)

    def remove_entry(self, index: int) -> None:
        """
        Deleting a todos by its index in the todos list

        Args:
            index: deleted index

        Exception:
            TodoIsNotExistError: todos with index does not exist
        """

        try:
            index = int(index)
        except ValueError as err:
            raise ValueError(f"Not int {index=} in remote_entry: {err}") from err

        try:
            self.entries.remove(self.entries[index])
        except IndexError:
            raise TodoIsNotExistError(f"Todo does not exist with index {index}."
                                      f" Total {len(self.entries)} todos.") from None

        new_data: TodoList = {
            "name": self.name,
            "todos": self.entries,
        }

        self._update(new_data)

    def _update(self, new_data: TodoList) -> None:
        """Function rewrite todos journal data to new"""
        with open(file=self.path_todo, mode="w", encoding='utf-8') as todo_file:
            json.dump(
                new_data,
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )

    def update_data(self) -> None:
        """Function rewrite todos journal data to"""
        self.entries = self._parse()["todos"]

    def _parse(self) -> TodoList:
        """Function read todos list

        Returns:
            Todos: todos

        Exceptions:
            PathTodoFileError: file is not exist
        """
        try:
            with open(file=self.path_todo, mode='r', encoding="utf-8") as todo_file:
                data = json.load(todo_file)
            return data
        except FileNotFoundError:
            raise PathTodoFileError(f"Не существует такой тудушки по пути: {self.path_todo}") \
                from None

    def _create(self):
        """Function create empty todos list"""
        with open(file=self.path_todo, mode="w", encoding='utf-8') as todo_file:
            json.dump(
                {"name": self.name, "todos": []},
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )


if __name__ == '__main__':
    todos = TodoJournal("../data1.json", "test")
    # todos.add_entry("Hello")
    # todos.add_entry("world")
    # todos.add_entry("Go")
    print(todos.validate_json())
